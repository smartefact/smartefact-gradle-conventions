/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    `kotlin-dsl`
    id("maven-publish")
}

group = "smartefact"
version = "0.3.1"

dependencies {
    api("biz.aQute.bnd:biz.aQute.bnd.gradle:6.4.0")
    api("com.diffplug.spotless:spotless-plugin-gradle:6.21.0")
    api("gradle.plugin.io.morethan.jmhreport:gradle-jmh-report:0.9.0")
    api("info.solidsoft.gradle.pitest:gradle-pitest-plugin:1.15.0")
    api("me.champeau.jmh:jmh-gradle-plugin:0.7.1")
    api("net.ltgt.gradle:gradle-errorprone-plugin:3.0.1")
    api("net.ltgt.gradle:gradle-nullaway-plugin:1.5.0")
    api("org.barfuin.gradle.jacocolog:gradle-jacoco-log:3.1.0")
    api("org.jetbrains.dokka:dokka-gradle-plugin:1.9.20")
    api("org.jetbrains.kotlin:kotlin-gradle-plugin:1.9.24")
    api("smartefact:smartefact.gradle.plugin.doxygen:0.3.0")
}

publishing {
    val gitlabGroup = "smartefact"
    val gitlabProjectName = "smartefact-gradle-conventions"
    val gitlabProjectId = "57800397"
    publications.withType<MavenPublication> {
        pom {
            name.set(project.name)
            description.set(project.description)
            url.set("https://gitlab.com/$gitlabGroup/$gitlabProjectName")
            licenses {
                license {
                    name.set("Apache License 2.0")
                    url.set("https://www.apache.org/licenses/LICENSE-2.0")
                }
            }
            organization {
                name.set("Smartefact")
                url.set("https://smartefact.be")
            }
            developers {
                developer {
                    id.set("lpireyn")
                    name.set("Laurent Pireyn")
                    email.set("laurent.pireyn@smartefact.be")
                }
            }
            scm {
                connection.set("https://gitlab.com/$gitlabGroup/$gitlabProjectName.git")
                developerConnection.set("git@gitlab.com:$gitlabGroup/$gitlabProjectName.git")
                url.set("https://gitlab.com/$gitlabGroup/$gitlabProjectName/-/tree/main")
            }
            ciManagement {
                system.set("gitlab")
                url.set("https://gitlab.com/$gitlabGroup/$gitlabProjectName/-/pipelines")
            }
        }
    }
    repositories {
        maven {
            name = "gitlab"
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class.java) {
                name = "Private-Token"
                value = System.getenv("GITLAB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
