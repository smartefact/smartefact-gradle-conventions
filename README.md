[![Latest release](https://gitlab.com/smartefact/smartefact-gradle-conventions/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact-gradle-conventions/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact-gradle-conventions/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact-gradle-conventions/-/commits/main)

# Smartefact Gradle conventions

A collection of highly opinionated [Gradle](https://gradle.org/) conventions used by Smartefact projects.
Use at your own risk.

## Conventions

### smartefact.convention.base

- Enhances the `base` plugin
- Sets the group to `smartefact` (opinionated)
- Makes archives reproducible (see [Reproducible Builds](https://reproducible-builds.org/))

### smartefact.convention.java

- Enhances the `java` plugin
- Adds support for Java sources and resources templates to each source set
- Uses [JetBrains annotations](https://github.com/JetBrains/java-annotations)
- Uses [Error Prone](https://errorprone.info/)
- Uses [NullAway](https://github.com/uber/NullAway) for the `smartefact` package (opinionated)
- Uses JVM test suites
- Uses [JUnit Jupiter](https://junit.org/junit5/) in all test source sets
- Uses [JUnit Pioneer](https://junit-pioneer.org/) in all test source sets
- Sets the temp directory to a subdirectory of the `build` directory in all tests
- Uses [JaCoCo](https://www.jacoco.org/jacoco/) in unit tests
- Prints a summary of the JaCoCo coverage for unit tests
- Uses [PIT](https://pitest.org/) for the `smartefact` package (opinionated)
- Improves the Javadoc
- Uses [Doxygen](https://www.doxygen.nl/)
- Builds the sources JAR
- Builds the Javadoc JAR
- Publishes to a Smartefact project on [GitLab](https://gitlab.com/smartefact/) (opinionated)

### smartefact.convention.java11

- Uses Java 11

### smartefact.convention.java17

- Uses Java 17

### smartefact.convention.java-library

- Enhances the `java-library` plugin

### smartefact.convention.kotlin-jvm

- Enhances the `smartefact.convention.java` convention for Kotlin JVM
- Adds support for Kotlin sources templates to each source set
- Uses [Dokka](https://github.com/Kotlin/dokka)

### smartefact.convention.osgi-bundle

- Enhances the `smartefact.convention.java-library` convention for an [OSGi](https://www.osgi.org/) bundle
- Uses [bnd](https://bnd.bndtools.org/)
- Configures the bundle manifest for Smartefact (opinionated)

### smartefact.convention.jmh

- Uses [JMH](https://github.com/openjdk/jmh)
- Generates JMH reports

### smartefact.convention.java-gradle-plugin

- Enhances the `java-gradle-plugin` plugin

### smartefact.convention.kotlin-gradle-plugin

- Enhances the `kotlin-gradle-plugin` plugin

### smartefact.convention.spotless

- Configures [Spotless](https://github.com/diffplug/spotless) for Smartefact conventions (opinionated)

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Roadmap

- Convention for [ProGuard](https://www.guardsquare.com/proguard)
- Convention for [yGuard](https://www.yworks.com/products/yguard)

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
