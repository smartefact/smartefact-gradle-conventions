# Changelog

This changelog is based on [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0).

This project adheres to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0).

## [0.3.1] - 2024-12-02

### Fixed

- Actually create publication in `smartefact.convention.java`

## [0.3.0] - 2024-09-08

### Added

- Added `org.junit.jupiter.api.BeforeAll` and `org.junit.jupiter.api.BeforeEach` to NullAway custom initializer annotations

### Removed

- Removed plugin `jdeprscan`

## [0.2.0] - 2024-09-02

### Added

- Created convention `smartefact.convention.kotlin-gradle-plugin`

## [0.1.0] - 2024-06-04

### Added

- Created convention `smartefact.convention.base`
- Created convention `smartefact.convention.java`
- Created convention `smartefact.convention.java11`
- Created convention `smartefact.convention.java17`
- Created convention `smartefact.convention.java-library`
- Created convention `smartefact.convention.application`
- Created convention `smartefact.convention.osgi-bundle`
- Created convention `smartefact.convention.spotless`
- Created convention `smartefact.convention.jmh`
- Created convention `smartefact.convention.kotlin-jvm`
- Created convention `smartefact.convention.java-gradle-plugin`
