/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.jetbrains.dokka.DokkaConfiguration
import org.jetbrains.dokka.gradle.DokkaTask

plugins {
    id("smartefact.convention.java")
    id("org.jetbrains.kotlin.jvm")
    id("org.jetbrains.dokka")
}

// Kotlin templates

gradle.projectsEvaluated {
    // The project group and version are often not set when this plugin is applied,
    // so this is deferred to after the projects have been evaluated
    // TODO: Try to use providers to avoid doing this in `gradle.projectsEvaluated`
    val templatesProperties = mapOf(
        "generator" to "gradle",
        "projectGroup" to project.group,
        "projectName" to project.name,
        "projectVersion" to project.version.toString()
    )
    // Add support for Kotlin templates to each source set
    sourceSets.all {
        // Add support for Java templates to source set
        val kotlinTemplatesSrcDir = layout.projectDirectory.dir("src/$name/kotlinTemplates")
        val kotlinTemplatesBuildDir = layout.buildDirectory.dir("generated/sources/kotlinTemplates/kotlin/$name")
        val kotlinTemplatesTask = tasks.create(getTaskName("process", "kotlinTemplates"), Sync::class) {
            description = "Processes the $name Kotlin templates."
            // Don't set the group, to mimic the related KotlinCompile task
            from(kotlinTemplatesSrcDir)
            into(kotlinTemplatesBuildDir)
            expand(templatesProperties)
            inputs.properties(templatesProperties)
        }
        kotlin.srcDir(kotlinTemplatesTask)
    }
}

// Dokka

tasks.withType<DokkaTask> {
    dokkaSourceSets.configureEach {
        documentedVisibilities.set(setOf(
            DokkaConfiguration.Visibility.PUBLIC,
            DokkaConfiguration.Visibility.PROTECTED
        ))
    }
}

val dokka by tasks.registering(DefaultTask::class) {
    description = "Generates Dokka documentation."
    group = JavaBasePlugin.DOCUMENTATION_GROUP
    dependsOn(
        tasks.dokkaHtml,
        tasks.dokkaJavadoc
    )
}

tasks.assemble {
    dependsOn(dokka)
}
