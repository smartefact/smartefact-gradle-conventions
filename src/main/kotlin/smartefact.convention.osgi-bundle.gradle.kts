/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.time.LocalDate

plugins {
    id("smartefact.convention.java-library")
    id("biz.aQute.bnd.builder")
}

val osgiAnnotationBundleVersion = "2.0.0"
val osgiAnnotationVersioningVersion = "1.1.2"

// Java

dependencies {
    compileOnly("org.osgi:org.osgi.annotation.bundle:$osgiAnnotationBundleVersion")
    compileOnly("org.osgi:org.osgi.annotation.versioning:$osgiAnnotationVersioningVersion")
}

// Testing

testing {
    jvmTestSuites {
        dependencies {
            compileOnly("org.osgi:org.osgi.annotation.bundle:$osgiAnnotationBundleVersion")
            compileOnly("org.osgi:org.osgi.annotation.versioning:$osgiAnnotationVersioningVersion")
        }
    }
}

// JAR

tasks.jar {
    bundle {
        val gitlabGroup = "smartefact"
        val gitlabProjectName = providers.gradleProperty("gitlab.projectName").get()
        val currentYear = LocalDate.now().year
        val inceptionYear = providers.gradleProperty("inceptionYear").orNull?.toInt() ?: currentYear
        val copyrightYears = inceptionYear.toString() + if (currentYear > inceptionYear) "-${currentYear}" else ""
        properties.set(provider { mapOf(
            "Bundle-Copyright" to "Smartefact (c) $copyrightYears",
            "Bundle-Description" to project.description.orEmpty(),
            "Bundle-Developers" to "lpireyn;name=\"Laurent Pireyn\";email=laurent.pireyn@smartefact.be;organization=Smartefact;organization-url=https://smartefact.be;roles=maintainer",
            "Bundle-License" to "Apache License 2.0;link=https://www.apache.org/licenses/LICENSE-2.0",
            "Bundle-SCM" to "connection=https://gitlab.com/$gitlabGroup/$gitlabProjectName.git,developer-connection=git@gitlab.com:$gitlabGroup/$gitlabProjectName.git,url=https://gitlab.com/$gitlabGroup/$gitlabProjectName/-/tree/main",
            "Bundle-Vendor" to "Smartefact"
        ) })
        bnd(
            "-noextraheaders true"
        )
    }
}

// Javadoc

tasks.withType<Javadoc> {
    standardOptions {
        links?.add("https://javadoc.io/doc/org.osgi/org.osgi.annotation.bundle/$osgiAnnotationBundleVersion")
        links?.add("https://javadoc.io/doc/org.osgi/org.osgi.annotation.versioning/$osgiAnnotationVersioningVersion")
    }
}
