/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.diffplug.gradle.spotless.FormatExtension
import com.diffplug.spotless.LineEnding

plugins {
    id("smartefact.convention.base")
    id("com.diffplug.spotless")
}

// Spotless

spotless {
    fun FormatExtension.applyDefaults() {
        endWithNewline()
        trimTrailingWhitespace()
    }

    encoding = Charsets.UTF_8
    lineEndings = LineEnding.GIT_ATTRIBUTES
    format("bat") {
        target("**/*.bat")
        applyDefaults()
    }
    java {
        applyDefaults()
        // Exclude generated sources annotated with @Generated
        targetExcludeIfContentContainsRegex("@(?:javax\\.annotation\\.(?:processing\\.)?)?Generated\\(")
        indentWithSpaces(4)
        removeUnusedImports()
        formatAnnotations()
    }
    kotlin {
        applyDefaults()
    }
    kotlinGradle {
        applyDefaults()
        ktlint()
    }
    format("properties") {
        target("**/*.properties")
        applyDefaults()
        encoding = Charsets.ISO_8859_1
    }
}
