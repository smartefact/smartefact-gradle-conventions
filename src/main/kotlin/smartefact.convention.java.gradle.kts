/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import net.ltgt.gradle.errorprone.errorprone
import net.ltgt.gradle.nullaway.nullaway
import org.gradle.internal.impldep.org.bouncycastle.asn1.x509.X509ObjectIdentifiers.organization

plugins {
    id("smartefact.convention.base")
    id("java")
    id("net.ltgt.errorprone")
    id("net.ltgt.nullaway")
    id("jacoco")
    id("org.barfuin.gradle.jacocolog")
    id("info.solidsoft.pitest")
    id("smartefact.doxygen")
    id("maven-publish")
}

val errorproneVersion = "2.27.1"
val jacocoVersion = "0.8.12"
val jetbrainsAnnotationsVersion = "24.1.0"
val junitVersion = "5.10.2"
val nullawayVersion = "0.10.9"

// Java templates

gradle.projectsEvaluated {
    // The project group and version are often not set when this plugin is applied,
    // so this is deferred to after the projects have been evaluated
    // TODO: Try to use providers to avoid doing this in `gradle.projectsEvaluated`
    val templatesProperties = mapOf(
        "generator" to "gradle",
        "projectGroup" to project.group,
        "projectName" to project.name,
        "projectVersion" to project.version.toString()
    )
    // Add support for Java and resources templates to each source set
    sourceSets.all {
        // Add support for Java templates to source set
        val javaTemplatesSrcDir = layout.projectDirectory.dir("src/$name/javaTemplates")
        val javaTemplatesBuildDir = layout.buildDirectory.dir("generated/sources/javaTemplates/java/$name")
        val javaTemplatesTask = tasks.create(getTaskName("process", "javaTemplates"), Sync::class) {
            description = "Processes the $name Java templates."
            // Don't set the group, to mimic the related JavaCompile task
            from(javaTemplatesSrcDir)
            into(javaTemplatesBuildDir)
            expand(templatesProperties)
            inputs.properties(templatesProperties)
        }
        java.srcDir(javaTemplatesTask)
        // Add support for resources templates to source set
        val resourcesTemplatesSrcDir = layout.projectDirectory.dir("src/$name/resourcesTemplates")
        val resourcesTemplatesBuildDir = layout.buildDirectory.dir("generated/sources/resourcesTemplates/resources/$name")
        val resourcesTemplatesTask = tasks.create(getTaskName("process", "resourcesTemplates"), Sync::class) {
            description = "Processes the $name resources templates."
            // Don't set the group, to mimic the related JavaCompile task
            from(resourcesTemplatesSrcDir)
            into(resourcesTemplatesBuildDir)
            expand(templatesProperties)
            inputs.properties(templatesProperties)
        }
        resources.srcDir(resourcesTemplatesTask)
    }
}

// Java

dependencies {
    compileOnly("com.google.errorprone:error_prone_annotations:$errorproneVersion")
    compileOnly("org.jetbrains:annotations:$jetbrainsAnnotationsVersion")
}

java {
    withSourcesJar()
    withJavadocJar()
}

tasks.withType<JavaCompile> {
    options.javaModuleVersion.set(provider { project.version.toString() })
}

// Error Prone

dependencies {
    errorprone("com.google.errorprone:error_prone_core:$errorproneVersion")
    errorprone("com.uber.nullaway:nullaway:$nullawayVersion")
}

tasks.withType<JavaCompile> {
    options.errorprone {
        isEnabled.set(true)
        ignoreSuppressionAnnotations.set(false)
        disableWarningsInGeneratedCode.set(false)
        // This pattern doesn't match my habits
        // https://errorprone.info/bugpattern/OperatorPrecedence
        disable("OperatorPrecedence")
        nullaway {
            error()
            annotatedPackages.add("smartefact")
            isAssertsEnabled.set(true)
            checkOptionalEmptiness.set(true)
            checkContracts.set(true)
            handleTestAssertionLibraries.set(true)
            suggestSuppressions.set(true)
            excludedFieldAnnotations.addAll(
                "org.junit.jupiter.api.io.TempDir",
                "org.mockito.Mock",
                "org.openjdk.jmh.annotations.Param",
                "picocli.CommandLine.Option"
            )
            customInitializerAnnotations.addAll(
                "org.junit.jupiter.api.BeforeAll",
                "org.junit.jupiter.api.BeforeEach",
                "org.openjdk.jmh.annotations.Setup"
            )
        }
    }
}

// Testing

testing {
    jvmTestSuites {
        dependencies {
            implementation(platform("org.junit:junit-bom:$junitVersion"))
            implementation("org.junit.jupiter:junit-jupiter")
            implementation("org.junit.jupiter:junit-jupiter-params")
            implementation("org.junit-pioneer:junit-pioneer:2.2.0")
            compileOnly("com.google.errorprone:error_prone_annotations:$errorproneVersion")
            compileOnly("org.jetbrains:annotations:$jetbrainsAnnotationsVersion")
            runtimeOnly("org.junit.platform:junit-platform-reporting")
        }
        targets.configureEach {
            testTask {
                useJUnitPlatform()
                // Enable assert keyword
                jvmArgs("-ea")
                systemProperty("java.io.tmpdir", layout.buildDirectory.dir("tmp/${name}").get().asFile.absolutePath)
                // Enable and configure Open Test Reporting reports in JUnit Platform
                systemProperty("junit.platform.reporting.open.xml.enabled", true)
                systemProperty("junit.platform.reporting.output.dir", java.testResultsDir.dir(name).get().asFile.absolutePath)
            }
        }
        tasks.getByName<JavaCompile>(sources.compileJavaTaskName) {
            options.errorprone {
                // Allow JUnit Jupiter @Nested inner classes
                // https://errorprone.info/bugpattern/ClassCanBeStatic
                disable("ClassCanBeStatic")
            }
        }
    }
}

// JaCoCo

jacoco {
    toolVersion = jacocoVersion
}

tasks.jacocoTestReport {
    reports {
        xml.required.set(true)
        html.required.set(true)
    }
    dependsOn(tasks.test)
}

tasks.test {
    finalizedBy(tasks.jacocoTestReport)
}

// PIT

pitest {
    junit5PluginVersion.set("1.2.1")
    targetClasses.set(setOf("smartefact.*"))
    jvmArgs.set(listOf("-ea"))
    threads.set(4)
    exportLineCoverage.set(true)
    outputFormats.set(setOf("XML", "HTML"))
    timestampedReports.set(false)
}

// Javadoc

tasks.withType<Javadoc> {
    standardOptions {
        version()
        author()
        linkSource()
        links?.add("https://javadoc.io/doc/com.google.errorprone/error_prone_annotations/$errorproneVersion")
        links?.add("https://javadoc.io/doc/org.jetbrains/annotations/$jetbrainsAnnotationsVersion")
    }
}

// Doxygen

tasks.assemble {
    dependsOn(tasks.doxygen)
}

// Publishing

publishing {
    val gitlabGroup = "smartefact"
    val gitlabProjectName = providers.gradleProperty("gitlab.projectName").get()
    val gitlabProjectId = providers.gradleProperty("gitlab.projectId").get()
    publications.create<MavenPublication>("main") {
        from(components["java"])
        pom {
            name.set(project.name)
            description.set(project.description)
            url.set("https://gitlab.com/$gitlabGroup/$gitlabProjectName")
            licenses {
                license {
                    name.set("Apache License 2.0")
                    url.set("https://www.apache.org/licenses/LICENSE-2.0")
                }
            }
            organization {
                name.set("Smartefact")
                url.set("https://smartefact.be")
            }
            developers {
                developer {
                    id.set("lpireyn")
                    name.set("Laurent Pireyn")
                    email.set("laurent.pireyn@smartefact.be")
                }
            }
            scm {
                connection.set("https://gitlab.com/$gitlabGroup/$gitlabProjectName.git")
                developerConnection.set("git@gitlab.com:$gitlabGroup/$gitlabProjectName.git")
                url.set("https://gitlab.com/$gitlabGroup/$gitlabProjectName/-/tree/main")
            }
            ciManagement {
                system.set("gitlab")
                url.set("https://gitlab.com/$gitlabGroup/$gitlabProjectName/-/pipelines")
            }
        }
    }
    repositories {
        maven {
            name = "gitlab"
            url = uri("https://gitlab.com/api/v4/projects/$gitlabProjectId/packages/maven")
            credentials(HttpHeaderCredentials::class.java) {
                name = "Private-Token"
                value = System.getenv("GITLAB_TOKEN")
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}
