/*
 * Copyright 2024 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

plugins {
    id("smartefact.convention.java")
    id("me.champeau.jmh")
    id("io.morethan.jmhreport")
}

// JMH

dependencies {
    jmh("org.openjdk.jmh:jmh-core:1.37")
}

jmh {
    threads
    // JMH report requires JSON results
    // Unfortunately, only one format can be used
    resultFormat.set("JSON")
}

val jmhReportDir = layout.buildDirectory.dir("reports/jmh/html")

jmhReport {
    jmhResultPath = tasks.jmh.get().resultsFile.get().asFile.absolutePath
    jmhReportOutput = jmhReportDir.get().asFile.absolutePath
}

tasks.jmh {
    finalizedBy(tasks.jmhReport)
}

tasks.jmhReport {
    doFirst {
        // Create output directory, as the task fails if it doesn't exist
        mkdir(jmhReportDir)
    }
}
